﻿using Microsoft.Xrm.Sdk;
using PredicaCRM.Common.Models;
using PredicaCRM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredicaCRM.Publisher
{
    public class ContactParser
    {
        public static Dictionary<int, string> genderMapping = new Dictionary<int, string>()
        {
            { (int)Contact_gendercode.Male, "Male" },
            { (int)Contact_gendercode.Female, "Female" }
        };

        public static ContactModel Parse(contact contact)
        {
            var genderStringValue = "";
            string creationDate;

            try
            {
                if (contact.gendercode?.Value != null) genderStringValue = genderMapping[contact.gendercode.Value];
                if (contact.predica_import_creation_date.HasValue) creationDate = contact.predica_import_creation_date.Value.ToString("dd.MM.yyyy");
            }
            catch (Exception ex)
            {
                throw new Exception("Lead parser error occured.", ex);
            }

            var model = new ContactModel()
            {
                OrdinalNumber = contact.predica_import_ordinal_number,
                FirstName = contact.firstname,
                LastName = contact.lastname,
                Email = contact.emailaddress1,
                Gender = genderStringValue,
                Country = contact.address1_country,
                Age = contact.predica_import_age.ToString(),
                Id = contact.predica_import_id
            };

            return model;
        }
    }
}
