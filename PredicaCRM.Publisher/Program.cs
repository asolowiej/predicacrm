﻿using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using PredicaCRM.Common.Consts;
using PredicaCRM.Common.Models;
using PredicaCRM.Core;
using PredicaCRM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredicaCRM.Publisher
{
    internal class Program
    {
        private static IQueueClient queueClient;

        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            try
            {
                var serviceBusConnectionString = ConfigurationManager.AppSettings["PredicaServiceBusConnectionString"];

                var queueName = ConfigurationManager.AppSettings["PredicaQueueName"];

                var predicaCRMConnectionString = ConfigurationManager.ConnectionStrings["PredicaCRMConnection"];

                var organizationService = new OrganizationServiceCreator().GetOrganizationService(predicaCRMConnectionString);

                List<contact> contactsToPublish = null;

                using (var context = new CrmServiceContext(organizationService))
                {
                    contactsToPublish = context.contactSet
                        .Where(x => x.address1_country == CountryNames.UnitedStates)
                        .Select(x => new contact
                        {
                            predica_import_ordinal_number = x.predica_import_ordinal_number,
                            firstname = x.firstname,
                            lastname = x.lastname,
                            emailaddress1 = x.emailaddress1,
                            gendercode = x.gendercode,
                            address1_country = x.address1_country,
                            predica_import_age = x.predica_import_age,
                            predica_import_creation_date = x.predica_import_creation_date,
                            predica_import_id = x.predica_import_id,
                        }).ToList();
                }

                queueClient = new QueueClient(serviceBusConnectionString, queueName);

                Console.WriteLine(queueClient.ClientId);

                foreach (var contact in contactsToPublish)
                {
                    Console.WriteLine($"Sending out {contact.contactid}");

                    var contactSerialized = JsonConvert.SerializeObject(ContactParser.Parse(contact));

                    var contactBytes = Encoding.ASCII.GetBytes(contactSerialized);

                    var message = new Message(contactBytes);

                    Console.WriteLine(queueClient.IsClosedOrClosing);

                    await queueClient.SendAsync(message);
                }

                Console.WriteLine("Press key to continue...");
                Console.ReadKey();

                await queueClient.CloseAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Contact publisher extension error has occured.", ex);
            }
        }
    }
}
