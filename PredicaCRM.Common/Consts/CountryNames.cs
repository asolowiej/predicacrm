﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredicaCRM.Common.Consts
{
    public static class CountryNames
    {
        public static readonly string UnitedStates = "United States";
        public static readonly string France = "France";
    }
}
