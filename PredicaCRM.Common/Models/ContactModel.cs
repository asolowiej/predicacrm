﻿using System.Runtime.Serialization;

namespace PredicaCRM.Common.Models
{
    [DataContract]
    public class ContactModel
    {
        [DataMember(Name = "No")]
        public string OrdinalNumber { get; set; }

        [DataMember(Name = "First Name")]
        public string FirstName { get; set; }

        [DataMember(Name = "Last Name")]
        public string LastName { get; set; }

        [DataMember(Name = "Email")]
        public string Email { get; set; }

        [DataMember(Name = "Gender")]
        public string Gender { get; set; }

        [DataMember(Name = "Country")]
        public string Country { get; set; }

        [DataMember(Name = "Age")]
        public string Age { get; set; }

        [DataMember(Name = "Creation Date")]
        public string CreationDate { get; set; }

        [DataMember(Name = "Id")]
        public string Id { get; set; }
    }
}
