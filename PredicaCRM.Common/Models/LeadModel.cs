﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace PredicaCRM.Common.Models
{
    [DataContract]
    public class LeadModel
    {
        [DataMember(Name = "No")]
        public int OrdinalNumber { get; set; }

        [DataMember(Name = "First Name")]
        public string FirstName { get; set; }

        [DataMember(Name = "Last Name")]
        public string LastName { get; set; }

        [DataMember(Name = "Email")]
        public string Email { get; set; }

        [DataMember(Name = "Gender")]
        public string Gender { get; set; }

        [DataMember(Name = "Country")]
        public string Country { get; set; }

        [DataMember(Name = "Age")]
        public string Age { get; set; }

        [DataMember(Name = "Creation Date")]
        private string serializedCreationDate { get; set; }

        public DateTime CreationDate { get; set; }

        [DataMember(Name = "Id")]
        public string Id { get; set; }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            try
            {
                if (serializedCreationDate == null)
                {
                    CreationDate = default;
                }
                else
                {
                    CreationDate = DateTime.ParseExact(serializedCreationDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Error occured while deserializing DateTime attribute. Attribute contains value: {serializedCreationDate}.", ex);
            }
        }
    }
}
