# PredicaCRM

> Klient poprosił Cię o przygotowanie mechanizmu cyklicznie
> importującego do CRM dane lead'ów pozyskanych podczas konferencji
> branżowej. Wszystkie rekordy powinny być przekonwertowane
> automatycznie do Kontaktu i zawierać wszystkie kolumny z pliku.

Zrealizowane zostało to jako aplikacja konsolowa, która przez kontekst CRM-owy tworzy Leady, uprzednio likwidując duplikaty po Id rekordów.
Początkowo dałem się nabrać na te duplikaty, ale je posprzątałem :)
Pomiędzy Lead a Contact zrobione zostały mapowania, dzięki którym wartości pomiędzy tymi encjami przenoszą się.

> Podczas konwersji/tworzeniu kontaktu system powinien obliczyć
> automatycznie rok urodzenia każdego z kontaktu przyjmując, że w
> kolumnie 'Age'; jest aktualny wiek danej osoby.

Konwersja na Contact odbywa się z poziomu workflowa, który po utworzeniu Lead'a kwalifikuje go na Contact.
Rok (_predica_import_date_of_birth_) to pole obliczalne (Calculated), które korzysta z formuły _SubtractYears(predica_import_age, Now())_.

> Na encji kontakt, powinna być dostępna informacja określająca, że
> zawarcie umowy będzie ryzykowne ze względu na wiek. Definiowane jest
> to warunkiem gdzie mniej niż 25 lat i więcej niż 50 lat to ryzyko
> zawarcia umowy.

Zostało to zrobione w javascripcie przy pomocy starej biblioteki _XrmServiceToolkit_. Wiem, że jest już przestarzała, oraz że Xrm.Page niebawem przestanie działać,
uznałem jednak, że pakowanie np. TypeScripta, do którego jestem przyzwyczajony na potrzeby zrobienia małego skryptu nie ma za bardzo sensu, można było też zrobić to trochę inaczej,
ale takie rozwiązanie było dla mnie najszybsze.

> Kontaktom ze Stanów Zjednoczonych proces sprzedaży prowadzony jest
> poza systemem CRM - w zewnętrznym serwisie, do którego dane (z encji
> Kontakt) przesyłane są na kolejkę zlokalizowaną w Azure Service Bus.

W zewnętrznej aplikacji, podobnie jak przy imporcie - wykorzystywany jest kontekst CRM do pobrania Contact'ów, parsowanie i tak dalej, niestety - jak pisałem w mailu - przy próbie
wykonania połączenia otrzymałem w zwrocie 404.

> Kontaktom z Francji, system CRM powinien wysłać maila dziękującego za
> udział w konferencji (mail nie musi być fizycznie wysłany poza CRM)

Mail wysyłany jest przy pomocy Workflowa (Send mail) po utworzeniu Contactu, jeśli Kraj = Francja.

Po usunięciu duplikatów z 5000 rekordów otrzymano 43, w tym:

- 20 rekordów ze Stanów Zjednoczonych
- 10 z Francji (wysłano maile)

Plugin, który znalazł się w repozytorium nie zawiera się w rozwiązaniu zadania, nie ma go po prostu w CRM'ie.