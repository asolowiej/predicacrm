using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using PredicaCRM.Core.Entities;

namespace PredicaCRM.Resources.Plugins.Lead
{
    public partial class Lead_PostCreate : BasePlugin
    {
        public Lead_PostCreate(string unsecureConfig, string secureConfig) : base(unsecureConfig, secureConfig)
        {
            // Register for any specific events by instantiating a new instance of the 'PluginEvent' class and registering it
            base.RegisteredEvents.Add(new PluginEvent()
            {
                Stage = eStage.PostOperation,
                MessageName = MessageNames.Create,
                EntityName = EntityNames.lead,
                PluginAction = ExecutePluginLogic
            });
        }
        public void ExecutePluginLogic(IServiceProvider serviceProvider)
        {
                var testVal = 1;

                
                throw new InvalidPluginExecutionException($"{testVal}, exception");
        }
    }
}
