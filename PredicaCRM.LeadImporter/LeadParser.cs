﻿using Microsoft.Xrm.Sdk;
using PredicaCRM.Common.Models;
using PredicaCRM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredicaCRM.LeadImporter
{
    public class LeadParser
    {
        public static Dictionary<string, Predica_lead_predica_gendercode> genderMapping = new Dictionary<string, Predica_lead_predica_gendercode>()
        {
            { "Male", Predica_lead_predica_gendercode.Male },
            { "Female", Predica_lead_predica_gendercode.Female }
        };

        public static lead Parse(LeadModel model)
        {
            OptionSetValue genderOptionSetValue;
            string odrinalNumber;
            int age;

            try
            {
                age = int.Parse(model.Age);
                odrinalNumber = model.OrdinalNumber.ToString();
                genderOptionSetValue = new OptionSetValue((int)genderMapping[model.Gender]);
            }
            catch (Exception ex)
            {
                throw new Exception("Lead parser error occured.", ex);
            }

            var lead = new lead()
            {                
                predica_import_ordinal_number = odrinalNumber,
                firstname = model.FirstName,
                lastname = model.LastName,
                emailaddress1 = model.Email,
                predica_gendercode = genderOptionSetValue,
                address1_country = model.Country,
                predica_import_age = age,
                predica_import_id = model.Id
            };

            return lead;
        }
    }
}
