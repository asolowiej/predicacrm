﻿using Newtonsoft.Json;
using PredicaCRM.Common.Models;
using PredicaCRM.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace PredicaCRM.LeadImporter
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var predicaCRMConnectionString = ConfigurationManager.ConnectionStrings["PredicaCRMConnection"];

                var organizationService = new OrganizationServiceCreator().GetOrganizationService(predicaCRMConnectionString);

                var predicaBlobUri = ConfigurationManager.AppSettings["PredicaAzureBlobUri"];

                var helper = new ImportHelper();

                var jsonData = helper.RetrieveLeadsFromBlob(new Uri(predicaBlobUri));

                var leads = JsonConvert.DeserializeObject<IEnumerable<LeadModel>>(jsonData);

                var leadsWithoutDuplicates = leads.GroupBy(x => x.Id)
                    .Select(x => {
                        return x.OrderBy(y => y.Id)
                                            .First();
                    });

                foreach (var lead in leadsWithoutDuplicates)
                {
                    Console.WriteLine($"Creating new record for {lead.Id}");

                    var leadParsed = LeadParser.Parse(lead);

                    organizationService.Create(leadParsed);
                }
            }
            catch (ConfigurationErrorsException cex)
            {
                throw new Exception("Configuration error has occured.", cex);
            }
            catch (JsonException jex)
            {
                throw new Exception("Deserialization error has occured.", jex);
            }
            catch (Exception ex)
            {
                throw new Exception("Lead importer extension error has occured.", ex);
            }

            Console.WriteLine("Press key to continue...");
            Console.ReadKey();
        }
    }
}
