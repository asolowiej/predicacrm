﻿using Microsoft.Azure.Storage.Blob;
using System;

namespace PredicaCRM.LeadImporter
{
    public class ImportHelper
    {
        public string RetrieveLeadsFromBlob(Uri blobUri)
        {
            try
            {
                var cloudBlockBlob = new CloudBlockBlob(blobUri);

                return cloudBlockBlob.DownloadText();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured while downloading blob.", ex);
            }
        }
    }
}
