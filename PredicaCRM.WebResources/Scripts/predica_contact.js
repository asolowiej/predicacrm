﻿///<script src="XrmServiceToolkit.min.js" type="text/javascript" />

function formOnLoad() {
    checkIfContractingIsRisky();
}

function formOnSave() {

}

function ageOnChange() {
    checkIfContractingIsRisky();
}

function checkIfContractingIsRisky() {
    Xrm.Page.ui.clearFormNotification("error_invalid_value_of_age_field");
    Xrm.Page.ui.clearFormNotification("info_conclusion_of_a_contract_is_risky");

    var age = Xrm.Page.getAttribute("predica_import_age").getValue();

    if (!age || age < 1) {
        Xrm.Page.ui.setFormNotification("Invalid value of 'Age' field.", "ERROR", "error_invalid_value_of_age_field");

        return;
    } else if (age < 25 || age > 50) {
        Xrm.Page.ui.setFormNotification("Conclusion of the contract with this contact can be risky.", "INFO", "info_conclusion_of_a_contract_is_risky");

        return;
    }
}
