﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace PredicaCRM.Core
{
    public class OrganizationServiceCreator
    {
        private IOrganizationService organizationService = null;

        public IOrganizationService GetOrganizationService(ConnectionStringSettings connectionString)
        {
            try
            {
                var crmConnection = new CrmConnection(connectionString);

                organizationService = new OrganizationService(crmConnection);

                if (organizationService == null) throw new Exception("Could not establish connection: impossible to set Organization Service.");

                var connectedUserId = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;

                if (connectedUserId == null) throw new Exception("WhoAmIRequest execution failure.");

                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Connection successful!");
                Console.ResetColor();

                return organizationService;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Connection failure!");
                Console.ResetColor();

                throw ex;
            }
        }
    }
}
